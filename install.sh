#!/bin/sh

# Adding repositories (for i3, i3-gaps)
add-apt-repository ppa:kgilmer/speed-ricer

sudo apt-get update

# Installing prequisites
sudo apt install curl
sudo apt install zsh
sudo apt install i3
sudo apt install vim
sudo apt install neofetch
sudo apt install nitrogen
sudo apt install i3-gaps
sudo apt install fonts-firacode
sudo apt install rofi
sudo apt install compton
sudo apt install fonts-firacode

# Install snap packages
sudo snap install spotify
sudo snap install discord

# Setup Rofi gruvbox theme
git clone https://github.com/bardisty/gruvbox-rofi ~/.config/rofi/themes/gruvbox
cp $HOME/dotfiles/rofi $HOME/.config/rofi/config

# Kitty setup
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
cp $HOME/dotfiles/kitty.conf $HOME/.config/kitty/kitty.conf

# zsh, oh-my-zsh, antigen
cp $HOME/dotfiles/.zshrc $HOME/.config/.zshrc
ln -s $HOME/.config/.zshrc $HOME/.zshrc
git clone https://github.com/ohmyzsh/ohmyzsh.git $HOME/.oh-my-zsh
curl -L git.io/antigen > $HOME/.antigen.zsh

cp $HOME/dotfiles/config $HOME/.config/i3/config

# Setup icons for terminal
git clone https://github.com/sebastiencs/icons-in-terminal.git
icons-in-terminal/install.sh
